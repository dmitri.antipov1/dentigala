import { Component, OnInit } from '@angular/core';
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private ms: ModalService) { }

  ngOnInit(): void {
  }

  openPersonalPolicy() {
    this.ms.togglePrivatePolicyState(true);
  }
}
