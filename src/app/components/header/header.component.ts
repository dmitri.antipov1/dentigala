import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isOpenMenu: Boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu(): void {
    if (!this.isOpenMenu) {
      this.isOpenMenu = true;
      window.scrollTo(0, 0);
    } else {
      this.isOpenMenu = false;
      window.scrollTo(0, 0);
    }
  }

  public navigate() {
    this.isOpenMenu = false;
  }
}
