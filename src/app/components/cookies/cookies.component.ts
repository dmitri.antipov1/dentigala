import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.css']
})
export class CookiesComponent implements OnInit {
  @Output() showCookiesEvent = new EventEmitter();
  isCookiesPolicy$ = this.ms.isOpenCookiesPolicy$;

  constructor(private ms: ModalService) { }

  ngOnInit(): void {
  }

  agreeWithCookies(b: boolean): void {
    localStorage.setItem('cookies', String(b));
    this.showCookiesEvent.emit();
  }

  openCookiesPolicy(): void {
    this.ms.toggleCookiesPolicyState(true);
  }
}
