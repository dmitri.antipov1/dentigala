import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ImagesService, LicenseImages } from '../../services/images.service';
import { Event } from '@angular/router';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent {
  public images = this.imagesService.licenseImages$;
  public imageCount: number = 0;
  public imageUrl: string = '';
  @ViewChild('container') container: ElementRef | undefined

  constructor(private imagesService: ImagesService, private renderer: Renderer2) { }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event: MouseEvent): void {
    if (!this.container?.nativeElement.contains(event.target)) {
      this.imageCount = 0;
      this.imageUrl = '';
      this.renderer.removeClass(document.body, 'isScroll');
    }
  }

  public openImage(image: LicenseImages) {
    this.imageCount = image.id;
    this.imageUrl = image.url;
    this.renderer.addClass(document.body, 'isScroll');
  }

}
