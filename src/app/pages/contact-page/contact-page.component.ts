import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ModalService } from 'src/app/services/modal.service';
import { BehaviorSubject } from 'rxjs';
import { LoaderService } from '../../services/loader.service';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css'],
})
export class ContactPageComponent implements OnInit {
  form: FormGroup;
  public isLoading$: BehaviorSubject<boolean>;
  public isSuccessfully$: BehaviorSubject<boolean> = this.cs.isSuccessfully$;
  public isError$: BehaviorSubject<boolean> = this.cs.isError$;

  constructor(
    private fb: FormBuilder,
    private cs: ContactService,
    private ms: ModalService,
    private ls: LoaderService,
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('[- +()0-9]+')]],
      message: ['', Validators.required],
      checkInput: [false, Validators.pattern('true')],
    })
    this.isLoading$ = this.ls.isLoading$;
  }

  ngOnInit(): void {
  }

  sendMessage() {
    if (this.form.valid) {
      this.cs.sendForm(this.form.value);
      this.form.reset();
    }
  }

  openPolicy() {
    this.ms.openAgreementsModal(true);
  }

}
