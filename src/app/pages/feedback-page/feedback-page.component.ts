import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalService} from "../../services/modal.service";
import { ContactService } from '../../services/contact.service';
import { LoaderService } from '../../services/loader.service';
import { BehaviorSubject } from 'rxjs';

export type FeedBackFormType = {
  name: string;
  email: string;
  message: string;
  checkInput: boolean;
}

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.css']
})
export class FeedbackPageComponent implements OnInit {
  form: FormGroup;
  public isLoading$ = this.ls.isLoading$;
  public isSuccessfully$: BehaviorSubject<boolean> = this.cs.isSuccessfully$;
  public isError$: BehaviorSubject<boolean> = this.cs.isError$;

  constructor(
    private ms: ModalService,
    private ls: LoaderService,
    private fb: FormBuilder,
    private cs: ContactService
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
      checkInput: [false, Validators.pattern('true')],
    })
  }

  ngOnInit(): void {
  }

  openPolicy(): void {
    this.ms.openAgreementsModal(true);
  }

  sendForm(): void {
    if (this.form.valid) {
      this.cs.sendFeedbackForm(this.form.value);
    }
  }

}
