import { Component, OnInit } from '@angular/core';
import {ModalService} from "../../services/modal.service";

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {

  constructor(private ms: ModalService) { }

  ngOnInit(): void {
  }

  openCookiesPolicy() {
    this.ms.toggleCookiesPolicyState(true);
  }

  openPersonalPolicy() {
    this.ms.togglePrivatePolicyState(true);
  }

  public openAgreementsPolicy() {
    this.ms.openAgreementsModal(true);
  }
}
