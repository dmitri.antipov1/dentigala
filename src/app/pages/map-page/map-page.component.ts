import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-map-page',
  templateUrl: './map-page.component.html',
  styleUrls: ['./map-page.component.css']
})
export class MapPageComponent implements OnInit {

  constructor(private ms: ModalService) { }

  ngOnInit(): void {
  }

  openCookiesModal() {
    this.ms.toggleCookiesPolicyState(true);
  }

  openAgreementsModal() {
    this.ms.openAgreementsModal(true)
  }

  openPersonalDataModal() {
    this.ms.togglePrivatePolicyState(true);
  }


}
