import { Component, OnInit } from '@angular/core';
import { SlideInterface } from '../../slider/types/slide.interface';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  public slides: SlideInterface[] = [
    {id: 1, url: 'assets/images/slide-1.jpeg', title: 'first'},
    {id: 2, url: 'assets/images/slide-2.jpeg', title: 'second'},
    {id: 3, url: 'assets/images/slide-3.jpeg', title: 'third'},
    {id: 4, url: 'assets/images/slide-4.jpeg', title: 'fourth'},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
