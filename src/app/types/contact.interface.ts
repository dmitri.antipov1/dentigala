export interface ContactInterface {
  name: string;
  phone: string;
  message: string;
  checkInput: boolean
}
