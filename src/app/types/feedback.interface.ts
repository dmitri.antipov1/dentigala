export interface FeedbackInterface {
  name: string;
  email: string;
  message: string;
  checkInput: boolean
}
