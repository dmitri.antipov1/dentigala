import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { LocationIconComponent } from './icons/location-icon/location-icon.component';
import { EmailIconComponent } from './icons/email-icon/email-icon.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ChatIconComponent } from './icons/chat-icon/chat-icon.component';
import { TeamIconComponent } from './icons/team-icon/team-icon.component';
import { ToothIconComponent } from './icons/tooth-icon/tooth-icon.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { PricesPageComponent } from './pages/prices-page/prices-page.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { FeedbackPageComponent } from './pages/feedback-page/feedback-page.component';
import { MapPageComponent } from './pages/map-page/map-page.component';
import { ModalComponent } from './components/modal/modal.component';
import { CookiesComponent } from './components/cookies/cookies.component';
import { PersonalPolicyComponent } from './components/personal-policy/personal-policy.component';
import { CookiesPolicyComponent } from './components/cookies-policy/cookies-policy.component';
import { ReactiveFormsModule } from "@angular/forms";
import { AgreementsComponent } from './components/agreements/agreements.component';
import { PhoneIconComponent } from './icons/phone-icon/phone-icon.component';
import { ClockIconComponent } from './icons/clock-icon/clock-icon.component';
import { SliderModule } from './slider/slider.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImagesComponent } from './components/images/images.component';
import { LoaderComponent } from './components/loader/loader.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    LocationIconComponent,
    EmailIconComponent,
    HeaderComponent,
    FooterComponent,
    ChatIconComponent,
    TeamIconComponent,
    ToothIconComponent,
    AboutPageComponent,
    PricesPageComponent,
    TeamPageComponent,
    ContactPageComponent,
    FeedbackPageComponent,
    MapPageComponent,
    ModalComponent,
    CookiesComponent,
    PersonalPolicyComponent,
    CookiesPolicyComponent,
    AgreementsComponent,
    PhoneIconComponent,
    ClockIconComponent,
    ImagesComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    SliderModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
