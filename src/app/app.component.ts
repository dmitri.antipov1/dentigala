import { Component, OnInit } from '@angular/core';
import {ModalService} from "./services/modal.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showCookies = true;
  isOpenPrivatePolicy$ = this.ms.isOpenPrivatePolicy$;
  isCookiesPolicy$ = this.ms.isOpenCookiesPolicy$;
  isAgreementsPolicy$ = this.ms.isOpenAgreementsPolicy$;

  constructor(private ms: ModalService) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('cookies')) {
      this.showCookies = false;
    }
  }
}
