import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import { MainPageComponent } from './pages/main-page/main-page.component';
import {AboutPageComponent} from "./pages/about-page/about-page.component";
import {ContactPageComponent} from "./pages/contact-page/contact-page.component";
import {FeedbackPageComponent} from "./pages/feedback-page/feedback-page.component";
import {PricesPageComponent} from "./pages/prices-page/prices-page.component";
import {TeamPageComponent} from "./pages/team-page/team-page.component";
import {MapPageComponent} from "./pages/map-page/map-page.component";

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainPageComponent, data: {animation: 'main'}},
  { path: 'about', component: AboutPageComponent, data: {animation: 'about'}},
  { path: 'contact', component: ContactPageComponent, data: {animation: 'contact'}},
  { path: 'feedback', component: FeedbackPageComponent, data: {animation: 'feedback'}},
  { path: 'map', component: MapPageComponent, data: {animation: 'map'}},
  { path: 'prices', component: PricesPageComponent, data: {animation: 'prices'}},
  { path: 'team', component: TeamPageComponent, data: {animation: 'team'}},
  {path: '404', redirectTo: 'main'},
  {path: '**', redirectTo: 'main'}
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
