import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothIconComponent } from './tooth-icon.component';

describe('ToothIconComponent', () => {
  let component: ToothIconComponent;
  let fixture: ComponentFixture<ToothIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToothIconComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ToothIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
