import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export type LicenseImages = {
  id: number;
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  public licenseImages$: BehaviorSubject<LicenseImages[]> = new BehaviorSubject<LicenseImages[]>([])
  private images: LicenseImages[] = [
    {id: 1, url: 'assets/liecense-1.jpg'},
    {id: 2, url: 'assets/liecense-2.jpg'},
    {id: 3, url: 'assets/liecense-3.jpg'},
    {id: 4, url: 'assets/rpn-1.jpg'},
    {id: 5, url: 'assets/rpn-2.jpg'},
  ]

  constructor() {
    this.licenseImages$.next(this.images);
  }
}
