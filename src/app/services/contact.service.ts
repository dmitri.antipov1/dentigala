import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ContactInterface } from '../types/contact.interface';
import { LoaderService } from './loader.service';
import { FeedbackInterface } from '../types/feedback.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  public isSuccessfully$ = new BehaviorSubject<boolean>(false);
  public isError$ = new BehaviorSubject<boolean>(false);

  constructor(private apiService: ApiService, private ls: LoaderService) { }

  sendForm(data: ContactInterface): void {
    this.ls.isLoading$.next(true);
    this.apiService.sendContactForm(data).subscribe(() => {
      setTimeout(() => {
        this.ls.isLoading$.next(false);
        this.isSuccessfully$.next(true);
        setTimeout(() => {
          this.isSuccessfully$.next(false);
        }, 2000)
      }, 500)
    }, (err) => {
      this.isError$.next(true);
      setTimeout(() => {
        this.isError$.next(false);
      }, 2000)
    });
  }

  sendFeedbackForm(data: FeedbackInterface): void {
    this.ls.isLoading$.next(true);
    this.apiService.sendFeedbackForm(data).subscribe(() => {
      setTimeout(() => {
        this.ls.isLoading$.next(false);
        this.isSuccessfully$.next(true);
        setTimeout(() => {
          this.isSuccessfully$.next(false);
        }, 2000)
      }, 500)
    }, (err) => {
      this.isError$.next(true);
      setTimeout(() => {
        this.isError$.next(false);
      }, 2000)
    });
  }
}
