import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  isOpenCookiesPolicy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isOpenPrivatePolicy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isOpenAgreementsPolicy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  toggleCookiesPolicyState(state: boolean): void {
    this.isOpenCookiesPolicy$.next(state);
  }

  togglePrivatePolicyState(b: boolean) {
    this.isOpenPrivatePolicy$.next(b);
  }

  openAgreementsModal(b: boolean) {
    this.isOpenAgreementsPolicy$.next(b);
  }

  reset(): void {
    this.isOpenCookiesPolicy$.next(false);
    this.isOpenPrivatePolicy$.next(false);
    this.isOpenAgreementsPolicy$.next(false)
  }
}
