import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactInterface } from '../types/contact.interface';
import { Observable } from 'rxjs';
import { FeedbackInterface } from '../types/feedback.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private feedbackFormApi = 'https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/c770843f53854bf3a152f49eddd86277';
  private contactFormApi = 'https://forms.xn--80abckcp3bls.xn--p1ai/ingress/form/aa29138c71bb050c1fa1349cb131c2ae';

  constructor(private http: HttpClient) { }

  sendContactForm(data: ContactInterface): Observable<any> {
    const formData = new FormData();
    formData.append('name', data.name);
    formData.append('phone', data.phone);
    formData.append('message', data.message);
    formData.append('checkInput', String(data.checkInput));
    return this.http.post(this.contactFormApi, formData);
  }

  sendFeedbackForm(data: FeedbackInterface): Observable<any> {
    const feedbackFormData = new FormData();
    feedbackFormData.append('name', data.name);
    feedbackFormData.append('email', data.email);
    feedbackFormData.append('message', data.message);
    feedbackFormData.append('checkInput', String(data.checkInput));
    return this.http.post(this.feedbackFormApi, feedbackFormData);
  }
}
